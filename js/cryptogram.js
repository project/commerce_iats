(function (Drupal, $) {

  var script = null;

  Drupal.behaviors.commerceIatsCryptogram = {
    attach: function (context) {
      $('div.commerce-iats-hosted', context).each(function () {
        initCryptogram(this);
      });
    },
    detach: function (context) {
      $('div.commerce-iats-hosted', context).each(function () {
        if (script) {
          script.parentNode.removeChild(script);
          script = null;
        }
      });
    }
  };

  /**
   * Initialize the cryptogram library with attributes from the hosted form.
   */
  function initCryptogram(el) {
    if (script) {
      return;
    }

    var s = document.createElement('script');
    s.type = 'text/javascript';
    // See comment in Drupal\commerce_iats\Rest\Gateway->baseUrl re: validation
    // vs production domain.
    s.src = 'https://secure.1stpaygateway.net/secure/PaymentHostedForm/Scripts/firstpay/firstpay.cryptogram.js';
    s.id = 'firstpay-script-cryptogram';
    s.dataset.transcenter = el.dataset.transcenter;
    s.dataset.processor = el.dataset.processor;
    s.dataset.type = el.dataset.type;
    // @TODO: attributes for additional items.
    document.body.appendChild(s);
    script = s;

    window.addEventListener('message', function (e) {
      // Get the sent data
      if (typeof e.data === 'object') {
        const data = e.data;
        const isFirstPayMsg = data.hasOwnProperty('firstpay') && data.firstpay === true;
        const gotNewCryptogram = data.hasOwnProperty('type') && data.type === 'newCryptogram';
        if (isFirstPayMsg && gotNewCryptogram) {
          // @TODO: handle failures:
          //  - to get cryptogram
          //  - to validate card
          $('#checkout-cryptogram').val(data.cryptogram);
        }
      }
    });
  }

}) (Drupal, jQuery);
