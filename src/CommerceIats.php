<?php

namespace Drupal\commerce_iats;

use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CommerceIats payment class.
 */
class CommerceIats {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Provides a means to store data between requests for anon users.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * CommerceIats constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The configuration factory service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempStoreFactory
   *   Provides a means to store data between requests for anon users.
   */
  public function __construct(ConfigFactoryInterface $configFactory, AccountInterface $currentUser, PrivateTempStoreFactory $tempStoreFactory) {
    $this->currentUser = $currentUser;
    $this->configFactory = $configFactory;
    $this->tempStore = $tempStoreFactory->get('commerce_iats');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Gets a vault ID for a user.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account, optional. Defaults to the current user.
   *
   * @return string
   *   A vault ID for the user.
   */
  public function getUserVaultId(AccountInterface $account = NULL) {
    if (!$account) {
      $account = $this->currentUser;
    }
    // To ensure that multiple anonymous users opting to "check out as guest"
    // don't end up with the same vault ID, set a unique session variable for
    // each of them, and append it to their vault ID. (We don't need to do this
    // for users who log in, as we know their Drupal IDs will be unique.)
    $userPostfix = '';
    if ($account->isAnonymous()) {
      if (empty($this->tempStore->get('uniqueAnonId'))) {
        $random = new Random();
        $randomString = $random->name(30, TRUE);
        $this->tempStore->set('uniqueAnonId', $randomString);
      }
      $userPostfix = ':' . $this->tempStore->get('uniqueAnonId');
    }
    return 'drupal:' . $this->getVaultIdPrefix() . ':' . $account->id() . $userPostfix;
  }

  /**
   * Gets the site's vault ID prefix.
   *
   * @return string
   *   The vault prefix.
   */
  public function getVaultIdPrefix() {
    return $this->configFactory
      ->get('commerce_iats.settings')
      ->get('vault_id_prefix');
  }

  /**
   * Sets a vault ID prefix for the site.
   *
   * @param string|null $prefix
   *   A prefix to use for vault IDs, or NULL to create a random prefix.
   */
  public function setVaultIdPrefix($prefix = NULL) {
    if ($this->getVaultIdPrefix()) {
      return;
    }

    if (!$prefix) {
      $random = new Random();
      $prefix = $random->name(10, TRUE);
    }

    $this->configFactory
      ->getEditable('commerce_iats.settings')
      ->set('vault_id_prefix', $prefix)
      ->save();
  }

}
