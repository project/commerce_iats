<?php

namespace Drupal\commerce_iats\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Iats Gateway Base.
 */
abstract class CommerceIatsGatewayBase extends OnsitePaymentGatewayBase {
  use StringTranslationTrait;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, MinorUnitsConverterInterface $minor_units_converter = NULL, RequestStack $request_stack) {
    // The only point of this implementation of __construct() is to allow us to
    // do dependency injection with $request_stack; everything else here is
    // passing right through to the parent's constructor.
    $this->requestStack = $request_stack;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'transcenter' => '',
      'processor' => '',
      'gateway_id' => '',
      'processing_type' => 'hosted',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['transcenter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Transaction Center ID'),
      '#description' => $this->t('Your transaction center ID number for your 1stPayGateway account.'),
      '#default_value' => $this->configuration['transcenter'],
      '#required' => TRUE,
    ];

    $form['processor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Processor ID'),
      '#description' => $this->t('The ID number for a MID/TID combination on your 1stPayGateway account.'),
      '#default_value' => $this->configuration['processor'],
      '#required' => TRUE,
    ];

    $form['gateway_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Key'),
      '#description' => $this->t('The merchant key for your 1stPayGateway account.'),
      '#default_value' => $this->configuration['gateway_id'],
      '#required' => TRUE,
    ];

    $form['processing_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Processing type'),
      '#description' => $this->t('The processing type to use for this gateway.'),
      '#default_value' => $this->configuration['processing_type'],
      '#required' => TRUE,
      '#options' => [
        'hosted' => $this->t('Hosted form'),
        'direct_submission' => $this->t('Direct submission'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['transcenter'] = $values['transcenter'];
      $this->configuration['processor'] = $values['processor'];
      $this->configuration['gateway_id'] = $values['gateway_id'];
      $this->configuration['processing_type'] = $values['processing_type'];
    }
  }

  /**
   * Determines if the payment gateway is operating with hosted form processing.
   *
   * @return bool
   *   Indicates if the payment gateway is operating with hosted form
   *   processing.
   */
  public function isHosted() {
    return $this->configuration['processing_type'] == 'hosted';
  }

  /**
   * Sets billing info into a data array.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param array $data
   *   Optional. An existing data array, or a new one will be generated.
   *
   * @return array
   *   Data array with billing info set.
   */
  protected function setPaymentMethodBillingInfo(PaymentMethodInterface $payment_method, array $data = []) {
    /** @var \Drupal\address\AddressInterface $address */
    $address = $payment_method->getBillingProfile()->get('address')->first();

    $data['ownerName'] = $address->getGivenName() . ' ' . $address->getFamilyName();
    $data['ownerStreet'] = $address->getAddressLine1();
    $data['ownerStreet2'] = $address->getAddressLine2();
    $data['ownerCity'] = $address->getLocality();
    $data['ownerState'] = $address->getAdministrativeArea();
    $data['ownerZip'] = $address->getPostalCode();
    $data['ownerCountry'] = $address->getCountryCode();
    $data['ownerEmail'] = $payment_method->getOwner()->getEmail() ?: $this->requestStack->getCurrentRequest()->get('contact_information')['email'];

    return $data;
  }

  /**
   * Formats an amount.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The raw amount.
   *
   * @return string
   *   The formatted amount.
   */
  protected function formatAmount(Price $amount) {
    /** @var \Drupal\commerce_price\CurrencyFormatter $currency_formatter*/
    $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
    return $currency_formatter->format(
      $amount->getNumber(),
      $amount->getCurrencyCode(),
      [
        'currency_display' => 'none',
        'minimum_fraction_digits' => '2',
        'maximum_fraction_digits' => '2',
      ]);
  }

  /**
   * Gets the commerce iATS rest gateway service.
   *
   * @return \Drupal\commerce_iats\Rest\GatewayInterface
   *   The commerce iATS rest gateway service.
   */
  protected function getGateway() {
    return \Drupal::service('commerce_iats.rest_gateway_factory')
      ->getGateway(
        $this->configuration['gateway_id'],
        $this->configuration['processor']
      );
  }

  /**
   * Gets the commerce iATS service.
   *
   * @return \Drupal\commerce_iats\CommerceIats
   *   The commerce iATS service.
   */
  protected function getCommerceIats() {
    return \Drupal::service('commerce_iats');
  }

}
