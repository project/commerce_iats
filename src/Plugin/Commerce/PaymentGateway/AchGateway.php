<?php

namespace Drupal\commerce_iats\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_iats\Exception\GenericPaymentGatewayException;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * IATS ACH gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_iats_ach",
 *   label = "iATS ACH",
 *   display_label = "iATS ACH",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_iats\PluginForm\AchPaymentMethodAddForm",
 *   },
 *   payment_method_types = {"commerce_iats_ach"},
 *   modes = {"live" = "Live"},
 *   js_library = "commerce_iats/cryptogram",
 * )
 */
class AchGateway extends CommerceIatsGatewayBase implements SupportsRefundsInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['ach_category' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['ach_category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ACH category'),
      '#description' => $this->t('Enter an ACH category as configured in your transaction center.'),
      '#default_value' => $this->configuration['ach_category'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['ach_category'] = $values['ach_category'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);
    $amount = $payment->getAmount();

    $account = $payment->getOrder()->getCustomer();
    $vault_key = $this->getCommerceIats()->getUserVaultId($account);

    $transaction_data = [
      'orderId' => $payment->getOrderId() . '-' . $this->time->getCurrentTime(),
      'transactionAmount' => $this->formatAmount($amount),
      'categoryText' => $this->configuration['ach_category'],
    ];

    try {
      $result = $this->getGateway()->firstPayAchDebit(
        $vault_key,
        $payment_method->getRemoteId(),
        $transaction_data
      );
      $payment->setState('completed');

      $payment->setRemoteId($result->referenceNumber);
      $payment->save();
    }
    catch (\Exception $e) {
      throw new GenericPaymentGatewayException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    $gateway = $this->getGateway();
    $account = $payment->getOrder()->getCustomer();
    $vault_key = $this->getCommerceIats()->getUserVaultId($account);
    $payment_method = $payment->getPaymentMethod();

    // Why this 2-step approach of achVoid, then firstPayAchCredit? It's because
    // if the refund is happening PRIOR TO POST TIME (4pm eastern on the day the
    // transaction was run), then void will work. Otherwise, we have to just
    // credit the account for the refund amount, without reference to the
    // original order.

    $data = [
      'refNumber' => $payment->getRemoteId(),
    ];

    try {
      $gateway->achVoid($data);
    }
    catch (\Exception $e) {
      $error_data = $e->getData();
      // TODO: Change the API response to be clearer when we can't void.
      // Right now, if a transaction is too old to void (i.e. it has passed the
      // post time of 4pm eastern time), trying the /achVoid endpoint gives you
      // an error, and $error_data->errorMessages[0] contains "Void Failed.
      // Transaction cannot be voided." I would like an error CODE to test
      // against, rather than just that string. (There is an
      // $error_data->data->referenceNumber member, but it appears to come back
      // empty.)
      if ($error_data && empty($e->validationHasFailed) && !empty($error_data->isError)) {
        $data = [
          'autoGenerateorderId' => true,
          'transactionAmount' => $this->formatAmount($amount),
          'categoryText' => $this->configuration['ach_category'],
        ];
        try {
          $gateway->firstPayAchCredit($vault_key, $payment_method->getRemoteId(), $data);
        }
        catch (\Exception $e) {
          throw new PaymentGatewayException('Unable to perform refund.');
        }
      }
      else {
        throw new PaymentGatewayException('Unable to perform refund.');
      }
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * Utility function to determine whether the last 4 chars of 2 strings match.
   *
   * @param $a
   *   First string.
   * @param $b
   *   Second string.
   *
   * @return bool|int
   */
  public function last4Matches($a, $b) {
    if (count_chars($a) < 4 || count_chars($b) < 4) {
      return -1;
    }
    return substr_compare($a, substr($b, -4), -4) === 0;
  }

  /**
   * Given the data from an incoming acct, finds that account in a vault.
   *
   * This method gets called when someone enters ACH information during
   * checkout, but iATS's API rejects them with the message "Bank account
   * already exists" (i.e., it already exists in the iATS ACH vault associated
   * with this user, as identified by their unique vault ID). In this situation,
   * we have to sift through the accts in the vault and find the one that
   * matches both the provided ABA (routing) and DDA (account) numbers. A tricky
   * bit: the VaultQueryAchRecord API call underlying $gateway->vaultAchQuery()
   * only tells us the last 4 of each, hence the use of the above last4Matches()
   * method.
   *
   * @param array $data
   *   The ACH data entered by the user.
   * @param string $vault_key
   *   Their unique ACH vault ID.
   *
   * @return int
   */
  public function findAccount(array $data, string $vault_key) {
    $gateway = $this->getGateway();
    try {
      $result = $gateway->vaultAchQuery($vault_key, $data);
      if (!empty($result->bankingRecords) && is_array($result->bankingRecords)) {
        foreach ($result->bankingRecords as $banking_record) {
          $aba_matches = $this->last4Matches($data['aba'], $banking_record->abaLast4);
          $dda_matches = $this->last4Matches($data['dda'], $banking_record->ddaLast4);
          $acct_type_matches = $data['accountType'] == $banking_record->accountType;
          if ($aba_matches && $dda_matches && $acct_type_matches) {
            return $banking_record->id;
          }
        }
      }
    }
    catch (\Exception $e) {
      throw new GenericPaymentGatewayException();
    }
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $gateway = $this->getGateway();
    $vault_key = $this->getCommerceIats()
      ->getUserVaultId($payment_method->getOwner());

    $data = $this->setPaymentMethodBillingInfo($payment_method);

    if ($this->isHosted()) {
      $data['achCryptogram'] = $payment_details['cryptogram'];
    }
    else {
      $data['aba'] = $payment_details['routing_number'];
      $data['dda'] = $payment_details['account_number'];
      $data['accountType'] = $payment_details['account_type'];
    }

    // Add bank account to the vault.
    try {
      $result = $gateway->vaultAchCreate($vault_key, $data);
      $id = $result->id;
    }
    catch (\Exception $e) {
      $api_response_data = $e->getData();
      // This is a terrible way to test whether the bank account already exists,
      // because it relies on this text never changing.
      if (!empty($api_response_data->errorMessages[0]) && $api_response_data->errorMessages[0] == 'Bank account already exists') {
        $id = $this->findAccount($data, $vault_key);
      }
      else {
        throw new GenericPaymentGatewayException();
      }
    }

    // Get the bank account details from the vault.
    try {
      $result = $gateway->vaultAchLoad($vault_key, $id);
    }
    catch (\Exception $e) {
      throw new GenericPaymentGatewayException();
    }

    $payment_method->account_number = $result->ddaLast4;
    $payment_method->account_type = $result->accountType;
    $payment_method->setRemoteId($id);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $gateway = $this->getGateway();
    $vault_key = $this->getCommerceIats()
      ->getUserVaultId($payment_method->getOwner());
    $gateway->vaultAchDelete($vault_key, $payment_method->getRemoteId());
    $payment_method->delete();
  }

}
