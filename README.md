# Commerce iATS Module for Drupal Commerce

A module to implement the iATS Payments payment processing services for Drupal's
Commerce suite.

http://iatspayments.com/

## Features

* iATS Payments Rest API integration
* Credit card payment processing
* Hosted-form compatibility
* Recurring payments

## Requirements

Commerce iATS depends on the Drupal Commerce suite's Payment module, which is 
pulled in through Composer.

As long as you install via Composer, requirements are taken care of.

### Installation

Please use Composer:
`composer require drupal/commerce_iats`

Then enable via admin or:
`drush en commerce_iats`

## Payment Methods

Credit card and ACH payments can be processed through the iATS payment gateway.
To configure a payment gateway to use for iATS:

1. Go to /admin/commerce/config/payment-gateways

2. Choose the "iATS" plugin.

3. Enter your iATS processor credentials.

4. Choose between the "Hosted form" to use the iATS hosted iframe or "Direct 
   submission" processing through Drupal.

5. Save your payment gateway.

The iATS payment gateway will now be available for use during checkout. For more
information on configuring your site's checkout functionality, see 
https://www.drupal.org/node/2969597 or the documentation provided by Drupal 
Commerce.

If you want to accept ACH payment, make sure you have a valid ACH category, or 
iATS will decline.

## Recurring Payments

Recurring payment functionality is provided via the Commerce Recurring Framework
module (https://www.drupal.org/project/commerce_recurring). Using this you can
configure recurring subscriptions and various billing schedules. See
https://www.drupal.org/node/2969595 for complete instructions on configuring 
recurring payments.
